;====================================================================
; Main.asm file generated by New Project wizard
;
; Created:   �� ��� 30 2021
; Processor: ATmega328P
; Compiler:  AVRASM (Proteus)
;====================================================================

;====================================================================
; DEFINITIONS
;====================================================================

;������������ �������:
;rjmp - ����������� �������
;ldi - ��������� ���������������� �������� ��������� � �������
;sts - ��������� ��������������� � ����
;out - �������� ������ �� �������� � ���� I/O
;sbrs - ���������� ���� ��� � �������� ����������

;====================================================================
; VARIABLES
;====================================================================
   .equ F_CPU   = 8000000		;������� ����������(AVR 8)
   .equ	baud	= 9600			;�������� �������� ������ �� UART 
   .equ	bps	= (F_CPU/16/baud) - 1	; �������� ������� ����� �������� � ������� UBRRN(�������� 182 datasheet)
   .equ period1 = 500
   .equ period2 = 1000
;====================================================================
; RESET and INTERRUPT VECTORS
;====================================================================

   ;���������� ������� ��������, ������ �������� �� �������� 74
   ;0x0000 RESET - ����� ���������� �� ������� �������� ��������� ��� ���������, � ������ ������ �������� �� ����� ������������ ������ Start
   ;0x001C TIMER0_COMPA Timer/Counter0 Compare Match A - ���������� ������������ �������� ������ "A"
   ;0x001E TIMER0_COMPB Timer/Counter0 Compare Match B - ���������� ������������ �������� ������ "B"
   ;��� �������� A � B ����� �������� ������2 � ������1

   .org $000        ; (RESET) 
   rjmp   Start
   .org 0x0016
   jmp TimerInterrupt

   .ORG   INT_VECTORS_SIZE
;====================================================================
; CODE SEGMENT
;====================================================================
;������ ������������ ��� ������������ ������� 2
timer2_data:
   .db	"Pong",13,00

;������ ������������ ��� ������������ ������� 1
timer1_data:
   .db	"Ping",13,00

;������������� �������
;�������� � ���� ��������� ������ A
initTimer0:
      
   ;�������� �������� � ������� Output Compare Register ������ A (�������� 144)
   ;�������� - 8, ��� 0x08
   ;������������ ����� ������ ���������� ����� ����� ������������ �� �������. ������ ������������ - 1 ������������
   ldi r16, 0x08
   ldi r17, 0x00
   sts OCR1AH, r17
   sts OCR1AL, r16
   
   ;����� B ��������

   ;����� ������ ���������� (�������� 145)
   ldi r16, (1<<OCF1A)
   sts TIFR1,r16 ; Clear TOV0/ Clear pending interrupts
   
   ;���������� ���������� ��� ���������� ��������� ������� A (�������� 144)
   ldi r16, (1<<OCIE1A)
   sts TIMSK1,r16 ; Enable Timer/Counter0 Overflow Interrupt
   
   ;��������� ������ 4 - |WGM12:1| |WGM11:0| |WGM10:0| |CTC| (�������� 141)
   ;��� CTC - Clear Timer on Compare, �� ���� ��� ���������� ����������
   ;�������� ������� ����� ������� � �������� ����� ����
   ;� ������������ ������� ��� TOP Value(��� �������� ������) ������������ �������� OCRA
   
   ;� �������� ��������� �������� ��������� ������� ������� ��������� c �������� 100 ��(�������� 116, ������� �� �������� 117)
   ;-----------------------------------------------------------------
   ;����������:
   ;�������� ������� ���������� - 8��� ��� 8 000 000 ������ � �������
   ;������������ �������� �������������� ������������ - 1024
   ;������������ �������� �������� - 65535 (������ �������� 16 ���)
   ;����� - 8 000 000 / 1024 = 7812 ������ � �������
   ;������������ �������� �������� ����� ���������� �������� �� 8 ������
   ;-----------------------------------------------------------------
   
   ldi r16,(1<<WGM12) | (1<<CS12) | (0<<CS11) | (1<<CS10)
   sts TCCR1B,r16 ; clkI/O/1024 (From prescaler)
   
   ret

initUART:

   ;�������� ������� ����� baud prescale ������������ � ������ 18
   ldi	r16,LOW(bps)			; �������� �������(LOW) ����� bps � ������� r16
   ldi	r17,HIGH(bps)			; �������� �������(HIGH) ����� bps � ������� r17
   
   ;�������� ��������� UBRRnL and UBRRnH �� �������� 204
   sts	UBRR0L,r16			; �������� �������(LOW) ����� bps �� �������� r16 � UBRR0L
   sts	UBRR0H,r17			; �������� �������(HIGH) ����� bps �� �������� r16 � UBRR0H
   
   ;�������� �������� UCSRnB (�������� 201)
   ldi	r16,(1<<RXEN0)|(1<<TXEN0)	; ��������� ����� ��� RXEN(Receiver Enable) � TXEN(Transmitter Enable) � ���������� � ������� r16
   sts	UCSR0B,r16			; �������� �������� �� �������� r16 � UCSR0B

   ret					; ���������� ���������
	
printString:
puts:
   ;���������� ���������� ������� ��� ��������. �� ����� �������� � ������� r16
   lpm	r16,Z+				; �������� ������� �� ������ �� ������ ������������� ��������� Z � ������� r16
   cpi	r16,$00				; �������� �� NULL
   breq	puts_end			; ������� � ����� ��������� ��� ����������� ������� NULL

;�������� ��������� �������� ����������� �����-----------------------------------------------------------------------------------
putc:	
   lds	r17,UCSR0A			; �������� �������� �� �������� UCSR0A � r17 (�������� 200)
   
   ;������� SBRS - ���������� ���� ��� � �������� ����������(�������� - Google)
   ;��������� - UDRE0: USART Data Register Empty, ��� �������� ����������� ��������� ������
   sbrs	r17,UDRE0			; �������� ���� �����������
   rjmp	putc				; ������� �� ����� putc. ����������� ������ ���� ��� �������� ����������� ��������� ������ �� ����������
;---------------------------------------------------------------------------------------------------------------------------------

   sts	UDR0,r16			; ������ ������� � ������� UDR0(�������� �����). ������ � ���� ������� �������� �������� ������. (�������� 200)
   rjmp	puts				; ������� �� ����� puts

puts_end:
	ret					; return from subroutine

;���������� ���������� ������� 2. ����� ������ ��� ������������ ���������� ������ A.	
TimerInterrupt:

   ;���������� ������ �������� � ����. ���������� ��� ����������� ������ �� ����������� ����������
   push r16
   in r16,SREG
   push r16
   ;---------------------------------------------------------------------------
   
   timer1print:
   
   ldi	r16,LOW(period1)			; �������� �������(LOW) ����� bps � ������� r16
   ldi	r17,HIGH(period1)			; �������� �������(HIGH) ����� bps � ������� r17

   cp r16, XL
   brne timer2print
   
   cp r17, XH
   brne timer2print
   
   ldi	ZL,LOW(2*timer1_data)		; �������� ������� ����� ������ ������ ��� ������� 1 
   ldi	ZH,HIGH(2*timer1_data)		; �������� ������� ����� ������ ������ ��� ������� 1 
   rcall printString			;����� ������ � UART
   
   jmp exit
   
   timer2print:
   
   ldi	r16,LOW(period2)			; �������� �������(LOW) ����� bps � ������� r16
   ldi	r17,HIGH(period2)			; �������� �������(HIGH) ����� bps � ������� r17

   cp r16, XL
   brne exit
   
   cp r17, XH
   brne exit
   
   ldi	ZL,LOW(2*timer2_data)		; �������� ������� ����� ������ ������ ��� ������� 2 
   ldi	ZH,HIGH(2*timer2_data)		; �������� ������� ����� ������ ������ ��� ������� 2 
   rcall printString			;����� ������ � UART
   ldi XL, 0x00
   ldi XH, 0x00
   
   exit:   
   LD r16, X+
   
   ;���������� ������ �������� �� �����
   pop r16
   out SREG,r16
   pop r16
   reti
   ;---------------------------------------------------------------------------

;���������� ���������� ������� 1. ����� ������ ��� ������������ ���������� ������ B.	
Timer1:

   ;���������� ������ �������� � ����. ���������� ��� ����������� ������ �� ����������� ����������
   push r16
   in r16,SREG
   push r16
   ;---------------------------------------------------------------------------
   
   ldi	ZL,LOW(2*timer1_data)		; �������� ������� ����� ������ ������ ��� ������� 2 
   ldi	ZH,HIGH(2*timer1_data)		; �������� ������� ����� ������ ������ ��� ������� 2 
   rcall printString			;����� ������ � UART
   
   ;���������� ������ �������� �� �����
   pop r16
   out SREG,r16
   pop r16
   reti
   ;---------------------------------------------------------------------------

;������ ������
Start:
   ldi XL, 0
   ldi XH, 0 
   

   ; ������������� �����
   ldi R16,Low(RAMEND)	
   out SPL,R16		
    
   ldi R16,High(RAMEND)
   out SPH,R16
   
   ;������������� � ������ UART
   rcall	initUART
   
   ;������������� � ������ Timer0
   rcall	initTimer0
  
   ;���������� ���������� � �������
   sei 
   
;����������� ����
Loop:   
   rjmp  Loop

;====================================================================
